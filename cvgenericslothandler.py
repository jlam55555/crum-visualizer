import webbrowser

# special widget for generic slot handling. In designer, this can also be the
# parent of miscellaneous widgets, e.g., statusbar_perm, since it is buggy with
# multiple root widgets
from PySide2.QtCore import Slot
from PySide2.QtWidgets import QWidget


class CVGenericSlotHandler(QWidget):

    def __init__(self, parent=None):
        super(CVGenericSlotHandler, self).__init__(parent)

    # open gitlab repo in browser
    @Slot()
    def goto_gitlab(self):
        webbrowser.open('https://gitlab.com/josephii34erdfcv/traffic-network')
