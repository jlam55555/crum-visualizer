# essentially an enum; used for goto_mode
class Page:
    PAGE_SELECT = 0
    PAGE_RUN_SIM = 1
    PAGE_VIEW_RES = 2
    PAGE_ABOUT = 3
