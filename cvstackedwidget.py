# custom stackedwidget with slots to change pages
from PySide2.QtCore import Slot
from PySide2.QtWidgets import QStackedWidget

from page import Page


class CVStackedWidget(QStackedWidget):

    def __init__(self, parent=None):
        super(CVStackedWidget, self).__init__(parent)

    @Slot()
    def goto_run_page(self):
        self.setCurrentIndex(Page.PAGE_RUN_SIM)

    @Slot()
    def goto_view_page(self):
        self.setCurrentIndex(Page.PAGE_VIEW_RES)

    @Slot()
    def goto_about_page(self):
        self.setCurrentIndex(Page.PAGE_ABOUT)
