# this is a copy of simulator5 for the visualizer
import os
from datetime import datetime
from functools import reduce
from itertools import product
from dateutil import tz
import pandas as pd
import multiprocessing as mp
import numpy as np
import matplotlib.pyplot as plt
import pickle

from .simtypes import Agent, Graph, Crum, Bench


# declare runtime vars (to be used when running standalone)
# formalize simulation vars
class Environment():

    def __init__(self,
                 environment='prod',
                 network='braess_crum_bpr',
                 print_round_interval=10,
                 cpu_count=None,
                 output_filename='braess_crum_bpr'):
        self.ENVIRONMENT = environment
        self.NETWORK = network
        self.PRINT_ROUND_INTERVAL = print_round_interval
        self.CPU_COUNT = cpu_count if cpu_count else mp.cpu_count()
        self.OUTPUT_FILENAME = output_filename


def simulate(agent_counts, route_opts, routes, round_count, road_params,
             change_percent, crum_use_percent, trip_names, latency, env):
    if env.ENVIRONMENT == 'dev':
        print(f'G{change_percent} P{crum_use_percent} SETTING UP SIMULATION')

    graph = Graph(routes, road_params, route_opts, latency, agent_counts, trip_names)
    crum = Crum(graph)

    # JL 8/11/19: make more python-ic
    agents = [Agent(trip, graph.get_routes(trip), crum_use_percent,
                    graph.get_routes(trip)[int(np.floor(i/agent_count*len(graph.get_routes(trip))))])
              for trip, agent_count in enumerate(agent_counts)
              for i in range(agent_count)]

    # JL 8/11/19: this is possible now that Road, Route, and Trip classes
    #             all have get_name, get_travellers, and get_cost functions
    entity_types = [('Road', graph.get_roads), ('Route', graph.get_routes),
                    ('Trip', graph.get_trips), ('Graph', lambda: [graph])]

    # JL 8/11/19: Collect all data in this new df. Here, set up the
    #             pd.MultiIndex with three hierarchies
    data_collect = pd.DataFrame(index=range(round_count),
        columns=reduce(lambda ind1, ind2: ind1.union(ind2), [
            pd.MultiIndex.from_product([
                [entity_name],
                [entity.get_name() for entity in get_entity()],
                ['Cost','Travellers']
            ], names=['Entity Type', 'Entity Name', 'Data'])
            for entity_name, get_entity in entity_types
        ]), dtype=np.float32)

    if env.ENVIRONMENT == 'dev':
        print(f'G{change_percent} P{crum_use_percent} BEGINNING SIMULATION')

    simulation_bench = Bench()
    round_bench = Bench()
    # MAIN SIMULATION LOOP
    for round_index, current_round in enumerate(range(round_count)):
        # DEBUGGING INFORMATION
        if env.ENVIRONMENT == 'dev'\
                and env.PRINT_ROUND_INTERVAL\
                and not round_index % env.PRINT_ROUND_INTERVAL\
                and round_index:
            print(f'G{change_percent} P{crum_use_percent} SIMULATION ROUND {round_index}')
            round_bench.bench_and_print("ELAPSED TIME FOR LAST {} ROUNDS".format(env.PRINT_ROUND_INTERVAL))

        # PUT AGENTS THROUGH SIMULATION
        for agent in agents:
            agent.choose_route(change_percent, crum)
            agent.receive_travel_cost()
            if agent.uses_crum():
                crum.update_crum(agent.get_route())

        # COLLECT DATA
        for entity_type, get_entity in entity_types:
            for entity in get_entity():
                data_collect[entity_type, entity.get_name(), 'Cost'].iat[current_round] = entity.get_cost()
                data_collect[entity_type, entity.get_name(), 'Travellers'].iat[current_round] = entity.get_travellers()

    sim_time = simulation_bench.bench_and_print("G{} P{} SIMULATION TIME".format(change_percent, crum_use_percent))

    # drop first 50 rounds (if p=0, drop first 150)
    drop_num = 50 if crum_use_percent else 150

    # calculate and join means, std., adding new level to multiindex ('Statistic')
    stats = pd.concat([
        data_collect[drop_num:].mean().to_frame().T,
        data_collect[drop_num:].std().to_frame().T,
    ], axis=1, keys=['Mean', 'Std.'], names=['Statistic'])

    # reorder levels, fix grouping, and add index for stats
    stats = stats.reorder_levels(['Entity Type', 'Entity Name', 'Data', 'Statistic'], axis=1)
    for level in range(3):
        stats = stats.reindex(stats.columns.levels[level], level=level, axis=1, copy=False)
    stats.set_index(pd.MultiIndex.from_tuples([(change_percent, crum_use_percent)], names=['G', 'P']), inplace=True)

    # metadata array -- can add any extra data here
    metadata = pd.DataFrame({'G{} P{} simulation time'.format(change_percent, crum_use_percent): [sim_time]})

    return data_collect, stats, metadata

# JH June 24, 2019 removed t as a parameter
def run(properties, filename=None, show=False, save=True, env=Environment(), cb=lambda:...):
    # JL 8/11/19: condensing code
    filename = filename.split('.xlsx')[0] + '.xlsx' if filename\
               else datetime.now(tz.tzutc()).strftime('%Y%m%dT%H%M%Sz_') + properties['name'] + '.xlsx'
    outfile = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'outputs', filename) if save\
              else None

    agent_counts = properties['agents']
    routes = properties['routes']
    route_opts = properties['optimums']
    round_count = properties['steps']
    road_params = properties['graph']
    trip_names = properties['trips']
    simulation_name = filename.split('.xlsx')[0]
    latency = properties['latency']
    values = list(product(properties['crum']['g'],
                          properties['crum']['p']))
    nodes = properties['nodes']
    params_list = [(agent_counts, route_opts, routes, round_count, road_params, *params, trip_names, latency, env)
                   for params in values]

    # let pool worker threads handle multiple simulations and aggregate results
    application_bench = Bench()
    with mp.Pool(env.CPU_COUNT) as p:
        results = p.starmap(simulate, params_list)
    total_simulation_time = application_bench.bench_and_print("TOTAL SIMULATION RUNNING TIME")

    # print results summary and save to excel
    # JL 8/9/19: transpose result so that it fits in excel
    if env.ENVIRONMENT == 'dev':
        print('WRITING TO OUTPUT FILE')

    writer = pd.ExcelWriter(outfile, engine='xlsxwriter')

    stats_sheet = pd.concat([stats for _, stats, _ in results])
    stats_sheet.T.style.to_excel(writer,
                                 sheet_name='{} stats'.format(simulation_name))

    # save round data to excel, and plot
    for raw_data, stats, _ in results:
        sim_name = 'G{} P{}'.format(*stats.index[0])
        raw_data.T.to_excel(writer, sheet_name=sim_name)

        # plot with mpl.plt if show (false for use with visualization)
        if show:
            plt_x_range = np.arange(round_count)
            for route in raw_data['Route'].columns.get_level_values(0):
                plt.plot(plt_x_range, raw_data['Route', route, 'Travellers'])
            plt.title('Car Flow Distribution {}'.format(sim_name))
            plt.xlabel('# of Rounds')
            plt.ylabel('# of Cars')
            plt.show()

    # save metadata to excel
    metadata = pd.DataFrame({'Date': [str(datetime.now())],
                             'Total simulation time': [total_simulation_time],
                             'CPU count': [env.CPU_COUNT]})
    metadata = pd.concat([metadata, *[meta for _, _, meta in results]], axis=1)
    metadata.to_excel(writer, sheet_name='metadata')

    writer.save()

    # calculate SO traveller counts for visualization
    # create and set graph to optimum configuration (SO)
    graph = Graph(routes, road_params, route_opts, latency, agent_counts,
                  trip_names)
    for road in graph.get_roads():
        road.traveller_count = road.optimum
        road.mark_changed()
    for route in graph.get_routes():
        route.traveller_count = route.optimum
        route.calculate_cost()

    # calculate opt_vals
    entity_types = [('Road', graph.get_roads), ('Route', graph.get_routes),
                    ('Trip', graph.get_trips), ('Graph', lambda: [graph])]
    opt_vals = {
        entity_type: {
            entity.get_name(): {
                'Travellers': entity.get_travellers(),
                'Cost': entity.get_cost()}
            for entity in get_entities()}
        for entity_type, get_entities in entity_types}

    # write to pickle for visualization
    pickle.dump({
        'stats': stats_sheet,
        'round_data': {(g, p): data
                       for (g, p), (data, _, _) in zip(values, results)},
        'metadata': metadata,
        'nodes': nodes,
        'opt_vals': opt_vals
    }, open(f'outputs/{simulation_name}.crumdata', mode='w+b'))

    if env.ENVIRONMENT == 'dev':
        print('DONE')

    # let caller know of completion (i.e., if this is run asynchronously)
    cb()
