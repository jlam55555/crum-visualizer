import time
import numpy as np
import random as rd
from functools import reduce

# JL 7/10/19 simple class for benchmark testing
# create a new instance for each separate benchmark
# usage: bench() to return time since last bench() call and update start timestamp
# usage: bench_and_print(label) does exactly same as bench and prints elapsed time
class Bench():

    def __init__(self, debug=True):
        self.start = time.time()
        self.debug = debug

    def bench(self):
        now = time.time()
        elapsed = now - self.start
        self.start = now
        return elapsed

    def bench_and_print(self, label):
        elapsed = self.bench()
        if self.debug:
            print(f"{label}: {elapsed}")
        return elapsed


class Agent():
    """This object chooses a road based on given probabilities."""

    def __init__(self, trip, routes, crum_use_percent, choice):
        self.routes = np.array(routes)
        self.route_count = len(routes)
        self.trip = trip
        self.last_choice = choice  # beginning route distribution is uniform
        self.route_travel_counts = np.zeros(self.route_count)
        self.historic_route_costs = np.zeros(self.route_count)
        self._uses_crum = rd.random() < crum_use_percent
        self.last_choice.add_travellers()

    def uses_crum(self):
        return self._uses_crum

    def get_trip(self):
        return self.trip

    def get_route(self):
        return self.last_choice

    def choose_route(self, change_percent, crum):
        next_choice = self.last_choice

        decide, explore = rd.random(), rd.random()
        if decide > change_percent:
            pass
        elif self._uses_crum:
            # JL 8/8/19: using new crum.get_min_crum w/ caching for performance boost
            #            slightly faster than vector operations in else case below but equivalent
            next_choice = self.routes[rd.choice(crum.get_min_crum(self.trip))]
        elif explore < 0.05:
            next_choice = np.random.choice(self.routes)
        else:
            # JL 8/9/19: concise using np vector operations; equivalent to above
            min_cost = np.amin(self.historic_route_costs)
            next_choice = self.routes[np.random.choice(np.where(self.historic_route_costs < 0.1 + min_cost)[0])]

        self.route_travel_counts[next_choice.tag] += 1

        # update info in route
        if next_choice != self.last_choice:
            self.last_choice.add_travellers(-1)
            next_choice.add_travellers()
            self.last_choice = next_choice

    # average of all rounds for each route
    def receive_travel_cost(self):
        new_cost = self.last_choice.calculate_cost()
        previous_cost = self.historic_route_costs[self.last_choice.tag]
        count = self.route_travel_counts[self.last_choice.tag]
        # JL 7/10/19: Simpler moving-average algo
        # see https://stackoverflow.com/questions/7370801/measure-time-elapsed-in-python
        self.historic_route_costs[self.last_choice.tag] = new_average_route_cost = \
            previous_cost + (new_cost - previous_cost) / count


""" JL 8/11/19: Notes on model entities (Road, Route, Trip, Graph):
For ease and consistency of use for data recording (and, therefore the
visualization), these classes all implement the following functions:
    - get_name              a unique identifier for the route
    - get_travellers        traveller count
    - get_cost              average cost, not overall cost
Similarly, all subentities of a Graph object will be available with:
    - graph.get_roads()
    - graph.get_routes()
    - graph.get_trips()
"""

class Road():
    """This class simulates road congestion."""

    def __init__(self, tag, start_node, end_node):
        self.tag = tag
        self.start_node = start_node
        self.end_node = end_node
        self.name = '{}-{}'.format(self.start_node, self.end_node)
        self.traveller_count = 0

        # JL 7/10/19 caching times to avoid recalculations
        self.cost = 0
        self.changed = True

        # JL 7/12/19 optimum for visualization
        self.optimum = 0

    def add_travellers(self, count=1):
        self.traveller_count += count

    def get_travellers(self):
        return self.traveller_count

    def get_name(self):
        return self.name

    def mark_changed(self):
        self.changed = True

    # JL 7/12/19 find optimum
    def calculate_optimum(self, graph):
        self.optimum = reduce(lambda sum, route: sum + (route.optimum if self in route.get_edges() else 0),
                              graph.get_routes(), 0)


class RoadLinear(Road):
    """This is sub-class of Road that uses linear latency function."""

    def __init__(self, tag, start_node, end_node, alpha, beta):
        super().__init__(tag, start_node, end_node)
        self.alpha = alpha
        self.beta = beta

    def get_cost(self):
        # JL 7/10/19 caching road costs for performance
        if self.changed:
            self.cost = self.alpha + self.traveller_count * self.beta
            self.changed = False
        return self.cost


class RoadTanh(Road):
    """This is sub-class of Road that uses tanh latency function."""

    def __init__(self, tag, start_node, end_node, length, num_lanes):
        super().__init__(tag, start_node, end_node)
        self.length = length
        self.num_lanes = num_lanes

    def get_cost(self):
        # JL 7/10/19 didn't implement cost caching here -- is tanh used?
        if self.traveller_count:
            return self.length / (np.tanh(self.length * self.num_lanes / self.traveller_count - 2.0) + np.tanh(2.0))
        return self.length / 2.0


class RoadBPR(Road):
    """This is sub-class of Road that uses BPR latency function."""

    alpha = 0.15
    beta = 4

    def __init__(self, tag, start_node, end_node, time, capacity):
        super().__init__(tag, start_node, end_node)
        self.time = time
        self.capacity = capacity

    def get_cost(self):
        # JH June 24, 2019 testing various values of alpha and beta
        # JL 7/10/19 caching road costs for performance, replaced np.power with **
        if self.changed:
            self.cost = self.time * (1 + RoadBPR.alpha * (self.traveller_count / self.capacity) ** RoadBPR.beta)
            self.changed = False
        return self.cost


class Route():
    """EDITED"""

    def __init__(self, tag, trip, nodes, road_names, optimum):
        self.tag = tag
        self.name = '-'.join(nodes)
        self.trip = trip
        self.optimum = optimum
        self.current_weight = 0
        self.cost = 0
        self.traveller_count = 0

        # JH update July 2 2019: no restriction on node names
        # JL 8/11/19: more python-ic way to get self.roads
        self.roads = [road_names[adj_nodes] for adj_nodes in zip(nodes, nodes[1:])]

    def add_travellers(self, count=1):
        self.traveller_count += count

        # JL 7/10/19 added for caching performance
        for road in self.roads:
            road.add_travellers(count)
            road.mark_changed()

    def reset_travellers(self):
        self.traveller_count = 0

    def calculate_cost(self):
        self.cost = reduce(lambda total_cost,road:road.get_cost()+total_cost, self.roads, 0)
        return self.cost

    def get_travellers(self):
        return self.traveller_count

    def get_travellers_difference(self):
        return self.traveller_count - self.optimum

    def get_edges(self):
        return self.roads

    def get_name(self):
        return self.name

    def get_trip(self):
        return self.trip

    def get_tag(self):
        return self.tag

    def get_cost(self):
        return self.cost


# JL 8/11/19: Added to encapsulate some of the behavior of trips
#             This is useful for keeping the same "entity structure"
#             as Roads and Routes, which is used to simplify datalogging
class Trip():

    def __init__(self, trip_name, traveller_count, routes):
        self.routes = routes
        self.travellers = traveller_count
        self.name = trip_name

    def get_travellers(self):
        return self.travellers

    def get_name(self):
        return self.name

    # define once for use in get_cost
    def _sum_cost(self, cost_sum, route):
        return cost_sum + route.get_cost() * route.get_travellers()

    # note: this returns AVERAGE cost (as do the other get_costs)
    def get_cost(self):
        return reduce(self._sum_cost, self.routes, 0) / self.travellers


# routes[trip][route] = ['a', 'b', 'c']
# self.edges = [road, road, road]
# self.routes[trip][route] = route
class Graph():
    """EDITED"""

    def __init__(self, routes, road_params, route_opts, latency, agent_counts, trip_names):
        self.edges = []
        self.routes = [[] for i in range(len(routes))]
        self.agent_counts = agent_counts
        self.travellers = sum(agent_counts)

        # June 24, 2019 JH Added latency as parameter
        # JL 8/11/19: neat-ening up commented code below
        road_classes = {'linear': RoadLinear, 'tanh': RoadTanh, 'bpr': RoadBPR}
        self.edges = [road_classes[latency](i, *params) for i, params in enumerate(road_params.itertuples(index=False))]

        road_names = {(r.start_node, r.end_node): r for r in self.edges}
        self.routes = [[Route(j, i, nodes, road_names, route_opts[i][j]) for j, nodes in enumerate(trip)]
                       for i, trip in routes.items()]

        # JL 7/12/19: calculate edge optimums (for visualization)
        for edge in self.edges:
            edge.calculate_optimum(self)

        # JL 8/11/19: add routes to trip for easier datalogging discard last trip
        # name ('Overall') -- that is changed to Graph, not a trip anymore
        trip_names = trip_names[:-1]
        self.trips = [Trip(trip_name, agent_count, trip_routes)
                      for trip_name, agent_count, trip_routes
                      in zip(trip_names, agent_counts, self.routes)]

        self.routes = np.array(self.routes)

    def get_routes(self, trip=None):
        return self.routes[trip] if trip is not None else np.hstack(self.routes)

    def get_route(self, trip, route):
        return self.routes[trip][route]

    # JL 8/11/19: get roads, trips
    def get_roads(self):
        return self.edges

    def get_trips(self):
        return self.trips

    # JL 8/11/19: implementing entity methods for consistency
    def get_name(self):
        return 'Overall'

    # can be calculated from trips, routes, or edges
    # choose trips because it is the least numerous
    def get_cost(self):
        return reduce(lambda sum, trip: sum + trip.get_cost() * trip.get_travellers(),
                      self.trips, 0) / self.travellers

    def get_travellers(self):
        return self.travellers

class Crum():
    """This object assigns roads points so that traffic converges to a system optimum.
    crum updates only when a traveller completes his or her journey and only for that route."""

    def __init__(self, graph):
        self.diffV = [np.zeros(len(trip.routes)) for trip in graph.get_trips()]
        self.min_diffV = [0] * len(graph.get_trips())
        self.min_diffV_routes = [set(range(len(trip.routes))) for trip in graph.get_trips()]

    def update_crum(self, route):
        trip = route.get_trip()
        diffV = self.diffV[trip]
        min_diffV = self.min_diffV[trip]
        min_diffV_routes = self.min_diffV_routes[trip]

        current_diffV = diffV[route.get_tag()] = route.get_travellers_difference()

        # JL 8/6/19: cache and return min rather than search array every time
        if current_diffV < min_diffV:
            self.min_diffV[trip] = current_diffV
            self.min_diffV_routes[trip] = {route.get_tag()}
        elif current_diffV > min_diffV:
            min_diffV_routes.discard(route.get_tag())
            if not self.min_diffV_routes[trip]:
                min_diffV = self.min_diffV[trip] = min(diffV)
                self.min_diffV_routes[trip] = {i for i in range(len(diffV)) if diffV[i] == min_diffV}
        else:
            min_diffV_routes.add(route.get_tag())

    # JL 8/6/19: cache and return min rather than search array; O(N)->~O(log N)
    def get_min_crum(self, trip):
        # faster to randomly choose from a set by converting to tuple rather than list
        # see: https://stackoverflow.com/questions/15837729/
        return tuple(self.min_diffV_routes[trip])
