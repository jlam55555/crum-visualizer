# CRUM Visualizer
## Visualizer for the CRUM traffic optimization algorithm

Main simulation code and details on the main repo: [@josephii34erdfcv/traffic-network][1]. Also, the secondary fork, with some additional developments: [@jlam55555/traffic-network][2].

---

### Tested dependency versions:

- Python 3.7.4 (conda)
- PySide2 5.13.0*
- matplotlib 3.1.1
- pandas 0.25.0
- numpy 1.16.4

<small>* There seems to be a problem with installing PySide2 5.13.0 directly via `conda install` -- it worked better with using `pip` in the conda environment.</small>

---

### Instructions

Copy the proper netlists (\*.ntl) over from the [main repository][1] into [./netlists/][3], and/or the proper simulation results (\*.xlsx) into [./outputs/][4].

To run a simulation from a ntl file, go to Action-&gt;Run simulation. Enter the simulation name (without .ntl), a list of comma-separated G and P values, and CPU count (default is all).

(Viewing a simulation is the next step to implement, description will be here soon.)

[1]: https://gitlab.com/josephii34erdfcv/traffic-network
[2]: https://gitlab.com/jlam55555/traffic-network
[3]: ./netlists
[4]: ./outputs
