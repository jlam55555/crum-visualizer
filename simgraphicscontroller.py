import pickle

from PySide2.QtCore import Slot, Signal, QTimer
from PySide2.QtWidgets import QLineEdit, QComboBox, QButtonGroup, QRadioButton,\
    QWidget, QSlider

from matplotlib.colors import hsv_to_rgb

from cvcustomchartwidget import CVCustomChartWidget


# SimGraphisController deals directly with the data from the output files
# (*.crumdata). Handles main network graph visualization. Delegates customizable
# chart handling to CVCustomChartWidget
class SimGraphicsController(QWidget):

    def __init__(self, graph, root):

        super(SimGraphicsController, self).__init__()

        # create place for drawing graph
        self.graph = graph
        self.root = root

        self.ax = self.graph.figure.subplots()
        self.ax.axis('off')
        self.ax.axis('equal')
        self.stats = None
        self.round_data = None
        self.roads = []
        self.road_lines = {}
        self.simulation_keys = []
        self.so_vals = None

        # self.current_* used to describe parameters for network graph;
        # current_network, current_sim_key, and current_round also carry over
        # to the custom charts
        self.current_network = ''
        self.current_sim_key = None
        self.current_round = 0
        self.current_metric = 'Travellers'
        self.current_relative_to = 'Absolute'

        self.timer = QTimer()
        self.timer.timeout.connect(self.advance_sim_handler)
        self.timer.setInterval(200)

    # function to update main network graph
    def update_network_graph(self):
        if self.stats is None:
            return

        # slice data; get appropriate metric for current round of current sim
        round_data = self.round_data[self.current_sim_key]['Road'] \
            .xs(self.current_metric, axis=1, level=1) \
            .iloc[self.current_round]
        round_max = round_data.max()

        # if relative to UE/SO, change round_data to difference between it and
        # UE value (will be scaled to fit in UE/SO later)
        if self.current_relative_to == 'UE/SO':
            # UE value is chosen empirically from p=0 stats
            ue_vals = self.stats['Road']\
                .xs(self.current_metric, axis=1, level=1)\
                .xs('Mean', axis=1, level=1)\
                .loc[(self.current_sim_key[0], 0)]
            round_data -= ue_vals

        for road, value in round_data.items():
            if self.current_relative_to == 'Absolute':
                # TODO: increase contrast with light-colored lines
                self.road_lines[road]\
                    .set_color(hsv_to_rgb((0.625, value / round_max, 1)))
            else:
                # TODO: fix color range to account for going outside of
                #       [min(UE, SO), max(UE, SO)] range
                ue_val = ue_vals[road]
                so_val = self.so_vals['Road'][road][self.current_metric]
                color = value / (so_val - ue_val) / 3
                color = max(0.0, min(1/3, color))
                self.road_lines[road].set_color(hsv_to_rgb((color, 1, 1)))
        self.graph.figure.canvas.draw_idle()

    round_changed = Signal(int)

    @Slot(int)
    def update_round_handler(self, new_current_round):
        self.current_round = new_current_round
        self.update_network_graph()

    @Slot(int)
    def sim_changed_handler(self, sim_index):
        self.current_sim_key = self.simulation_keys[sim_index]
        self.update_network_graph()

    @Slot(QRadioButton)
    def metric_changed_handler(self, button):
        self.current_metric = button.text()
        self.update_network_graph()

    @Slot(QRadioButton)
    def relative_to_changed_handler(self, button):
        self.current_relative_to = button.text()
        self.update_network_graph()

    @Slot()
    def toggle_play_handler(self):
        if self.timer.isActive():
            self.timer.stop()
        else:
            self.timer.start()

    @Slot()
    def advance_sim_handler(self):
        # TODO: don't hardcode in number of rounds?
        self.current_round = (self.current_round + 1) % 200
        self.round_changed.emit(self.current_round)
        self.update_network_graph()

    @Slot()
    def open_sim_handler(self):
        filename = self.root.findChild(QLineEdit, 'sim_filename_input').text()
        self.current_network = filename

        # get output data file (outputs/*.crumdata), unload with pickle
        # these correspond directly with the pickle dump at the end of
        # simulator.py -- consult there for more details
        # TODO: do error checking on filename
        file_data = pickle.load(open(f'outputs/{filename}.crumdata', 'rb'))
        self.stats = file_data['stats']
        self.round_data = file_data['round_data']
        self.so_vals = file_data['opt_vals']
        nodes = file_data['nodes']

        # clear old graphs
        self.ax.lines = []

        # get a list of roads (by nodes)
        self.roads = list({(road_name.split('-')[0], road_name.split('-')[1])
            for road_name in self.stats['Road'].columns.get_level_values(0)})

        # map plot objects to string names in excel file
        self.road_lines = {
            f'{nd1}-{nd2}': self.ax.plot([nodes[nd1][0], nodes[nd2][0]],
                                         [nodes[nd1][1], nodes[nd2][1]])[0]
            for nd1, nd2 in self.roads}
        self.ax.relim()
        self.ax.autoscale_view()

        # update combobox with sim_keys
        self.simulation_keys = list(self.round_data.keys())
        sim_keys_combobox = self.root.findChild(QComboBox, 'sim_keys_combobox')
        sim_keys_combobox.clear()
        for g, p in self.simulation_keys:
            sim_keys_combobox.addItem(f'G{g} P{p}', (g, p))

        self.current_sim_key = self.simulation_keys[0]
        self.update_network_graph()

    @Slot()
    def add_chart_handler(self):
        if self.round_data is None:
            return

        # new_chart*, entity_select, entity_type, metric, relative_to, span
        # defaults:
        # entity_select: ALL
        # entity_type: roads # easy to update
        # metric: travellers # easy to update
        # relative_to: ABSOLUTE
        # span: current_round
        # TODO: customize
        nc = 'new_chart_'
        metric = self.root.findChild(QButtonGroup, f'{nc}metric')\
            .checkedButton().text()
        entities = self.root.findChild(QButtonGroup, f'{nc}entity_select') \
            .checkedButton().text()
        entity_type = self.root.findChild(QButtonGroup, f'{nc}entity_type') \
            .checkedButton().text()
        relative_to = self.root.findChild(QButtonGroup, f'{nc}relative_to')\
            .checkedButton().text()
        span = self.root.findChild(QButtonGroup, f'{nc}span') \
            .checkedButton().text()

        chart = CVCustomChartWidget(self.current_network, self.current_sim_key,
                                    metric, entity_type, entities, relative_to,
                                    span, self.current_round, self.round_data)

        # connect signals
        chart.request_data.connect(lambda: chart.new_data_draw(self.data))
        chart.request_round.connect(
            lambda: chart.update_round_and_redraw(self.current_round))
        self.root.findChild(QSlider, 'round_slider')\
            .valueChanged.connect(chart.update_round_and_redraw)
        # TODO: connect change sim/network signals

        self.root.findChild(QWidget,'chart_container').layout().addWidget(chart)
