import pickle
import sys
import multiprocessing

from PySide2.QtCore import QFile, QCoreApplication, Qt, Slot, Signal
from PySide2.QtUiTools import QUiLoader
from PySide2.QtWidgets import QApplication, \
    QSlider, QStatusBar, QLabel, QMainWindow, QWidget, QPushButton, QAbstractButton, QComboBox, QRadioButton, \
    QButtonGroup

from cvgenericslothandler import CVGenericSlotHandler
from cvrunslothandler import CVRunSlotHandler
from cvstackedwidget import CVStackedWidget
from simgraphicscontroller import SimGraphicsController

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


# custom component setup without requiring subclassing (which is more tedious);
# use this function for more ui-heavy logic, but keep other logic (e.g.,
# simulation/data logic) elsewhere
def setup_components(root):

    # set cpu count slider to be number of cpus
    num_cpus = multiprocessing.cpu_count()
    cpu_count_slider = root.findChild(QSlider, 'run_cpu_slider')
    cpu_count_slider.setMaximum(num_cpus)
    cpu_count_slider.setSliderPosition(num_cpus)

    # add permanent label to statusbar
    root.findChild(QStatusBar, 'statusbar')\
        .addPermanentWidget(root.findChild(QLabel, 'statusbar_perm_label'))

    # handle graphics logic (network graph, charts) elsewhere
    graph = FigureCanvas(Figure())
    root.findChild(QWidget, 'graph_container').layout().addWidget(graph)
    # TODO: populate vs_round_container
    sgc = SimGraphicsController(graph, root)

    # event listeners for graph
    # TODO: figure out why this doesn't work with instance methods (needs at
    #       least one lambda); if both lambdas, okay; if one lambda and one
    #       not, still okay; if neither lambda, problem (?!?)
    #       VERY STRANGE BEHAVIOR (but not critical b/c this tmp. fix works)
    root.findChild(QPushButton, 'open_sim_button')\
        .clicked.connect(lambda: sgc.open_sim_handler())
    root.findChild(QSlider, 'round_slider')\
        .valueChanged.connect(sgc.update_round_handler)
    root.findChild(QComboBox, 'sim_keys_combobox')\
        .currentIndexChanged.connect(sgc.sim_changed_handler)
    root.findChild(QButtonGroup, 'graph_metric')\
        .buttonClicked.connect(sgc.metric_changed_handler)
    root.findChild(QButtonGroup, 'graph_relative_to') \
        .buttonClicked.connect(sgc.relative_to_changed_handler)
    root.findChild(QPushButton, 'add_chart_button') \
        .clicked.connect(sgc.add_chart_handler)

    # controls for playing simulation
    root.findChild(QPushButton, 'toggle_play_button') \
        .clicked.connect(sgc.toggle_play_handler)
    root.findChild(QPushButton, 'advance_sim_button') \
        .clicked.connect(sgc.advance_sim_handler)
    # terrible indenting but long lines :(
    root.findChild(QSlider, 'round_slider') \
        .valueChanged.connect(Slot(int)(
        lambda cr: root.findChild(QLabel, 'current_round_label').setNum(cr+1)))
    sgc.round_changed.connect(root.findChild(QSlider, 'round_slider').setValue)


if __name__ == '__main__':
    # this line to suppress an error; probably not too important
    QCoreApplication.setAttribute(Qt.AA_ShareOpenGLContexts)

    # start app
    app = QApplication(sys.argv)

    # load app using uiloader
    window = QMainWindow()
    loader = QUiLoader()
    file = QFile("main.ui")
    file.open(QFile.ReadOnly)

    # register custom components (mostly for custom slots/signals)
    loader.registerCustomWidget(CVGenericSlotHandler)
    loader.registerCustomWidget(CVRunSlotHandler)
    loader.registerCustomWidget(CVStackedWidget)

    window = loader.load(file)
    file.close()

    # do custom setup on components (this is for adding small behaviors to
    # elements without needing to subclass it)
    setup_components(window)

    # start app
    window.show()
    sys.exit(app.exec_())
