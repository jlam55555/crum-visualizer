from PySide2.QtCore import Signal, Slot
from PySide2.QtWidgets import QWidget, QVBoxLayout, QPushButton

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import numpy as np


class CVCustomChartWidget(QWidget):

    request_data = Signal()
    request_round = Signal()

    def __init__(self, net_name, sim_name, metric, entity_type, entities,
                 relative_to, span, current_round, data, parent=None):
        super(CVCustomChartWidget, self).__init__(parent)

        # set graph params as properties
        self.net_name = net_name
        self.sim_name = sim_name
        self.metric = metric
        self.entity_type = entity_type
        self.entities = entities
        self.relative_to = relative_to
        self.span = span
        self.current_round = current_round

        # hold data to draw (pd df)
        self.data = None

        # special drawing elements
        self.chart_entities = None
        self.round_marker_line = None

        # frozen prevents updates
        self.frozen = False

        # add ui elements
        # TODO: implement freeze/unfreeze ui elements (pushbutton? radios?)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        self.closeButton = QPushButton('Close chart')
        self.closeButton.clicked.connect(self.close)
        self.layout.addWidget(self.closeButton)
        self.chart = FigureCanvas(Figure())
        # arbitrary height -- maybe fix later?
        self.setFixedHeight(300)
        self.layout.addWidget(self.chart)
        self.ax = self.chart.figure.subplots()

        # fetch data and draw
        self.new_data_draw(data)

    # this is used for updating within the current simulation; only works when
    # unfrozen
    @Slot(int)
    def update_round_and_redraw(self, current_round):
        if self.frozen or current_round == self.current_round:
            return

        self.current_round = current_round
        if self.span == 'All rounds':
            self.round_marker_line.set_xdata(
                (self.current_round, self.current_round))
        else:
            round_data = self.data.iloc[self.current_round]
            for bar, value in zip(self.chart_entities, round_data):
                bar.set_height(value)
        self.chart.figure.canvas.draw()

    # this initiates requesting data for the updating sim process; this is used
    # when the simulation / network changes (or when unfreezing); only works
    # when unfrozen
    @Slot()
    def update_sim_and_draw(self):
        if not self.frozen:
            self.request_data.emit()

    # this uses the data response requested from update_sim_and_draw; this
    # should only be called from the sim graphics controller and from ctor
    def new_data_draw(self, data):
        self.data = data[self.sim_name][self.entity_type][:] \
            .xs(self.metric, axis=1, level=1)

        # TODO: get min/max of data
        # TODO: for relative, get min of data; for absolute, don't
        # print(self.data)
        # print(self.data.max().max())
        series_max = self.data.max().max()

        if self.span == 'All rounds':
            self.chart_entities = self.ax.plot(self.data)
            self.round_marker_line, = self.ax.plot(
                (self.current_round, self.current_round), (0, series_max), ':c')
        else:
            round_data = self.data.iloc[self.current_round]
            self.chart_entities = self.ax.bar(range(len(round_data)),round_data)
            self.ax.set_ylim((0, series_max * 1.1))

        self.chart.figure.canvas.draw()

    # freezing prevents further graph updates
    def freeze(self):
        self.frozen = True

    # unfreezing allows further graph updates and causes a graph (network and
    # round) update
    def unfreeze(self):
        self.frozen = False
        self.update_round_and_redraw()
        self.request_round()
