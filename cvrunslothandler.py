import os
from math import isnan
from threading import Thread

from PySide2.QtCore import Slot, Signal
from PySide2.QtWidgets import QWidget

from simulator.simulator import Environment, run
from simulator import netlist4 as nl


# special widget for run form slot handling
class CVRunSlotHandler(QWidget):

    run_form_error = Signal(str)
    run_form_valid = Signal()
    statusbar_message = Signal(str)
    statusbar_clear = Signal()

    def __init__(self, parent=None):
        super(CVRunSlotHandler, self).__init__(parent)
        self.env = Environment(print_round_interval=None,
                               cpu_count=None,
                               environment='prod')
        self.error = {
            "netlist": False,
            "g": False,
            "p": False,
            "cpu": False,
            "output": False
        }

        # default g, p (arbitrarily chosen -- subject to change)
        self.g_vals = [0.4]
        self.p_vals = [0, 0.6, 0.9]

    # called when simulation done
    def _finish_sim(self):
        self.statusbar_clear.emit()

    # modified from simulator __main__ method
    @Slot()
    def run_sim(self):
        # property validation
        for field, has_error in self.error.items():
            if has_error:
                self.run_form_error.emit(f'Error in field "{field}". Please '
                                         'fix before proceeding.')
                return

        # run simulation with given parameters
        self.run_form_valid.emit()
        properties = nl.load(self.env.NETWORK)
        properties['crum'] = {'g': sorted(self.g_vals),
                              'p': sorted(self.p_vals)}
        self.statusbar_message.emit(f'Running simulation g={self.g_vals}, '
                                    f'p={self.p_vals}...')

        # run in separate thread
        self.sim_thread = Thread(target=run, kwargs={
            'properties': properties,
            'filename': self.env.NETWORK,
            'show': False,
            'save': True,
            'env': self.env,
            'cb': self._finish_sim
        })
        # TODO: kill thread on QApplication exit; make a daemon?
        # TODO: data reporting, progress bar (for better UX, not critical)
        self.sim_thread.start()

    @Slot(str)
    def set_netlist(self, netlist_string):
        if os.path.exists(f'./netlists/{netlist_string}.ntl'):
            self.env.NETWORK = netlist_string
            self.error['netlist'] = False
        else:
            self.error['netlist'] = True

    # helper function for set_g and set_p
    # will throw exception if invalid
    def _float_list_from_string(self, string, includeZero=False):
        vals = list(filter(
            lambda val: not isnan(val) and val > 0 and val <= 1,
            [float(val) for val in string.split(',')]))
        if len(vals) != len(string.split(',')):
            raise Exception()
        return vals if not includeZero else list({*vals, 0})

    @Slot(str)
    def set_g(self, g_string):
        try:
            self.g_vals = self._float_list_from_string(g_string, False)
            self.error['g'] = False
        except:
            self.error['g'] = True

    @Slot(str)
    def set_p(self, p_string):
        try:
            self.p_vals = self._float_list_from_string(p_string, True)
            self.error['p'] = False
        except:
            self.error['p'] = True

    @Slot(int)
    def set_cpu_count(self, cpu_count):
        # not really a need for validation
        self.env.CPU_COUNT = cpu_count

    @Slot(str)
    def set_output(self, output_string):
        # maybe validate later
        self.env.OUTPUT_FILE = output_string
